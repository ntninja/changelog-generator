#!/usr/bin/python3
import json
import urllib.parse

import aiohttp
import kyoukai


urlencode = lambda s: urllib.parse.quote(s, safe="")


class GitHubClient:
	def __init__(self, client, domain="github.com"):
		domain = domain.encode("idna").decode("ascii")
		assert urlencode(domain) == domain
		
		self.__client   = client
		self.__api_base = "https://api.{0}/".format(domain)

	async def _fetch(self, url, **params):
		params.setdefault("headers", {"Accept": "application/vnd.github.v3+json"})
		async with self.__client.get(url, **params) as resp:
			resp.raise_for_status()
			return json.loads(await resp.text())

	async def fetch_name(self, entity, repository):
		url = "{0}repos/{1}/{2}".format(self.__api_base, urlencode(entity), urlencode(repository))
		return (await self._fetch(url))["name"]

	async def fetch_releases(self, entity, repository):
		url = "{0}repos/{1}/{2}/releases".format(self.__api_base, urlencode(entity), urlencode(repository))

		for release in await self._fetch(url):
			yield (release["tag_name"], release["body"])


async def generate_changelog(api_client, *args):
	# Send BOM to ensure UTF-8 encoding for plaintext file
	yield "\uFEFF"
	
	name = await api_client.fetch_name(*args)
	async for (version, body) in api_client.fetch_releases(*args):
		title = "{0} {1}".format(name, version)
		
		lines = []
		lines.append(title)
		lines.append("=" * len(title))
		lines.append("")
		lines.append(body)
		lines.append("")
		yield "\n".join(lines)


class ChangelogRouteGroup(kyoukai.routegroup.RouteGroup):
	def __init__(self, loop):
		self._http_client = aiohttp.ClientSession(loop=loop)
	
	def __del__(self):
		self._http_client.close()
	
	@kyoukai.routegroup.route('/github/<string:host>/<string:entity>/<string:repository>', methods=["GET"])
	async def github_generator(self, ctx: kyoukai.HTTPRequestContext, host: str, entity: str, repository: str):
		github_client = GitHubClient(self._http_client, host)
		
		# Doesn't look like this can be streamed currently
		sections = []
		async for section in generate_changelog(github_client, entity, repository):
			sections.append(section)
		return "\n".join(sections)
