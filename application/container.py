#!/usr/bin/python3
import asyncio

import asphalt
import kyoukai

from . import changelog


app = kyoukai.Kyoukai("changelog-generator")
app.root.add_route_group(changelog.ChangelogRouteGroup(asyncio.get_event_loop()))


class AppContainer(asphalt.core.ContainerComponent):
	async def start(self, ctx: asphalt.core.Context):
		self.add_component("kyoukai", kyoukai.KyoukaiComponent, app=app)
		
		return await super().start(ctx)
