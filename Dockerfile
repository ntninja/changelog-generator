FROM python:3-alpine

# Install build dependencies
RUN apk update
RUN apk add --virtual .build-deps gcc musl-dev

# Install web framework
RUN pip install --no-cache-dir --no-binary=all kyoukai aiohttp

# Remove build dependencies
RUN apk del .build-deps

ADD . /app
WORKDIR /app

EXPOSE 8080
CMD ["env", "PYTHONPATH=/app", "python3", "-O", "/usr/local/bin/asphalt", "run", "config.yml"]
